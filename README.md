Setup Instructions:
- project was developed on Linux Ubuntu 16.10, that's only platform that was tested on
- MySQL server and python >=2.7.x needs to be installed on local machine
- setup python packages so that all dependencies will be satisfied, see pip-freeze.txt for list of packages


Packages can be installed using virtualenv tool, see
https://stackoverflow.com/questions/18966564/pip-freeze-vs-pip-list

- create mysql user and DB locally with credentials as in settings:

CREATE DATABASE shortster CHARACTER SET utf8 COLLATE utf8_bin;

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'shortster',
        'USER': 'newuser',
        'PASSWORD': 'password',
        'HOST': 'localhost',
    }
}

- run from command line migrations that will populate DB according to models and framework settings:

python manage.py migrate

- create superuser :

python manage.py createsuperuser
for example admin, newpassword

- Run service:

python manage.py runserver

- Run model & functional tests:

python manage.py test

USAGE:

Use CURL to use the routes against the service: http://127.0.0.1:8000/

For example python snippet using Django client:

self.client.post('/shortcode_create', {'url': '/treleMorele', 'short_code': '123'})

response = self.client.get('/1234')

response = self.client.get('/1234/stats')


Assumptions:
- It's critical to prioritize atomicity of shortcodes, it's performance of creation, redirection time.
- User submitted shortcodes will be limited to X characters for several reasons:
  - predactibility of performance for search operation, for example 100 is reasonable limit which is easy to index in SQL DB indices and lookup, yet enough for user to express himself
  - URL's length is usually limited by the browsers
- For this demonstration database is setup with 10K sample codes generated and indexed
- System generated shortcodes can accomodate only limited slots of possible shortcodes, so we need prepare for possibility of overflowing the system
- When user submit a URL+shortcode and that shortcode is already taken, system will assign generated one.
- Duplicates of URL's are allowed (adding 2+ different shortcodes to same URL)
- URL submitted must be in correct format
- URL submitted must be less than ZZ characters, to not DDOS storage system, in this case I arbitraly set limit to 2000 characters
- URL's should not be one of sort codes, otherwise it will create loop of infinite redirections (not implemented though)

