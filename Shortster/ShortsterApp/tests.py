# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import IntegrityError, DataError
from django.test import TransactionTestCase, override_settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from ShortsterApp.models import *
from django.db.models import *
from django.test import Client
from multiprocessing import Pool
from django.utils import timezone

def async_client_post(options):
    client = Client()
    try:
        response = client.post(options['url'], options['data'])
    except Exception, e:
        return {'exception': str(e)}

    return {
        'status_code': response.status_code,
        'content': response.content,
    }

if __name__ == 'ShortsterApp.tests':
    class ModelTests(TransactionTestCase):
        fixtures = ['basicModelsDump.json']
        timeout = 2

        def test_valid_shortcode_created(self):
            shortcode = ShortCode(url='http://somewhere.com/landing_url/', short_code='SomethingNiceAndShort')
            shortcode.save()
            self.assertIsNotNone(shortcode.id)

        def test_duplicate_shortcode_exception(self):
            shortcode = ShortCode(url='http://somewhere.com/landing_url/', short_code='SomethingNiceAndShort')
            shortcode.save()

            shortcode2 = ShortCode(url='http://somewhere.com/landing_url/', short_code='SomethingNiceAndShort')
            with self.assertRaises(IntegrityError):
                shortcode2.save()

        def test_available_code_to_use(self):
            record_short_code = ShortCode.objects.filter(used__isnull=True).first()
            self.assertEqual('qqqqqq', record_short_code.short_code)


    @override_settings(DEBUG=True)
    class MyServerTests(StaticLiveServerTestCase):
        fixtures = ['basicModelsDump.json']
        timeout = 5
        client = None

        @classmethod
        def setUpClass(cls):
            cls.client = Client()

        @classmethod
        def tearDownClass(cls):
            pass

        def test_shortcode_create_explicit(self):
            response = self.client.post('/shortcode_create',
                                        {'url': 'http://somewhere.com/landing_url', 'short_code': '1234'})
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content, '1234')

        def test_shortcode_create_generated(self):
            short_code = ShortCode(short_code='123456')
            short_code.save()
            response = self.client.post('/shortcode_create', {'url': 'http://somewhere.com/landing_url'})
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content, '123456')

        def test_shortcode_invalid_format_regenerates(self):
            short_code = ShortCode(short_code='723456')
            short_code.save()
            response = self.client.post('/shortcode_create',
                                        {'url': 'http://somewhere.com/landing_url', 'short_code': '*!&#^*'})
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content, '723456')

        def test_url_invalid_format_error(self):
            short_code = ShortCode(short_code='723456')
            short_code.save()
            response = self.client.post('/shortcode_create', {'url': 'thisIsNotUrl', 'short_code': '1234'})
            self.assertEqual(response.status_code, 400)

        def test_url_too_long_error(self):
            short_code = ShortCode(short_code='723456')
            short_code.save()
            long_string = "a" * 10000
            try:
                self.client.post('/shortcode_create',
                                 {'url': 'http://somewhere.com/landing_url?q=' + long_string, 'short_code': '1234'})
            except DataError:
                pass

        def test_shortcode_too_short_regenerates(self):
            short_code = ShortCode(short_code='723456')
            short_code.save()
            response = self.client.post('/shortcode_create',
                                        {'url': 'http://somewhere.com/landing_url', 'short_code': '123'})
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content, '723456')

        def test_shortcode_create_explicit_but_generated(self):
            short_code = ShortCode(short_code='923456')
            short_code.save()
            response = self.client.post('/shortcode_create',
                                        {'url': 'http://somewhere.com/landing_url', 'short_code': '923456'})
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content, '923456')

        def test_redirect_shortcode_not_existing(self):
            response = self.client.get('/NotExistingButValidFormat')
            self.assertEqual(response.status_code, 404)
            self.assertEqual(response.content, 'NOT_FOUND')

        def test_redirect_shortcode_valid(self):
            time_before_test = timezone.now()
            self.test_shortcode_create_explicit()
            response = self.client.get('/1234')
            self.assertEqual(response.status_code, 302)
            self.assertEqual(response.content, '')
            self.assertEqual(response['Location'], 'http://somewhere.com/landing_url')
            record_short_code = ShortCode.objects.filter(short_code='1234').first()
            self.assertEqual(1, record_short_code.count_redirection)
            self.assertGreater(record_short_code.last_redirection, record_short_code.created_at)
            self.assertGreater(record_short_code.created_at, time_before_test)
            self.assertGreater(record_short_code.last_redirection, time_before_test)

        def test_redirect_shortcode_stats(self):
            self.test_redirect_shortcode_valid()
            response = self.client.get('/1234/stats')
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.content.find('"created_at"'))
            self.assertTrue(response.content.find('"last_redirection"'))

        def test_concurrency_parallel_create(self):
            for i in range(1, 10):
                short_code = ShortCode(short_code=str(i)*6)
                short_code.save()

            p = Pool(10)

            array_options = []
            options_first = {
                'url': '/shortcode_create',
                'data': {'url': 'http://somewhere.com/landing_url', 'test_delay' : '5' },
            }
            array_options.append(options_first)

            for i in range(1, 3):
                options_second = {
                    'url': '/shortcode_create',
                    'data': {'url': 'http://somewhere.com/landing_url'},
                }
                array_options.append(options_second)

            results = p.map(async_client_post, array_options)
            short_codes_unique = sorted(set([x['content'] if 'content' in x else x['exception'] for x in results]))

            self.assertEqual(['111111', '222222', '333333'], short_codes_unique)
