# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import re
from django.db import models

class ShortCode(models.Model):
    url = models.CharField(max_length=2000, null=True, blank=True)
    used = models.NullBooleanField(null=True, blank=True, db_index=True)
    short_code = models.CharField(max_length=100, null=False, blank=False, db_index=True, unique=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    last_redirection = models.DateTimeField(null=True, blank=True)
    count_redirection = models.IntegerField(null=False, blank=True, default=0)

    def __unicode__(self):
        return '%s' % self.short_code

    @classmethod
    def is_valid(cls, short_code):
        if len(short_code) < 4:
            return False
        if len(short_code) > 100:
            return False
        if not re.match('^[a-zA-Z0-9]+$', short_code):
            return False

        return True
