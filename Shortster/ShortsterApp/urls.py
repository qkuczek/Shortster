from ShortsterApp import views
from django.conf.urls import url, include

urlpatterns = [
    url(r'^shortcode_create$', views.shortcode_create, name='shortcode_create'),
    url(r'^(?P<short_code>[a-zA-Z0-9]+)/stats$', views.shortcode_stats, name='shortcode_stats'),
    url(r'^(?P<short_code>[a-zA-Z0-9]+)$', views.shortcode_redirect, name='shortcode_redirect'),
]
