# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-30 23:27
from __future__ import unicode_literals

from django.db import migrations

import itertools


def forwards_func(apps, schema_editor):
    possible_chars = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789'
    chars_count = 6
    generation_limit = 10000

    ShortCode = apps.get_model('ShortsterApp', 'ShortCode')

    count_generated = 0
    for possible_code_tuple in itertools.product(possible_chars, repeat=chars_count):
        possible_code = ''.join(possible_code_tuple)
        short_code = ShortCode(short_code=possible_code)
        short_code.save()
        count_generated+=1
        if count_generated == generation_limit:
            break


def reverse_func(apps, schema_editor):
    pass

class Migration(migrations.Migration):

    dependencies = [
        ('ShortsterApp', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
