# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseBadRequest
from django.core import serializers
from ShortsterApp.models import *
from django.http import HttpResponseRedirect
from django.db.models import *
from django.db.models import F
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
import time
from django.utils import timezone
from django.conf import settings
from django.db import transaction

@transaction.atomic
def shortcode_create(request):
    url = request.POST['url']
    validator = URLValidator()

    try:
        validator(url)
    except ValidationError:
        return HttpResponseBadRequest('URL_VALIDATION_FAILED')

    short_code = None
    record_short_code = None
    if 'short_code' in request.POST:
        short_code = request.POST['short_code']
        query_short_code = ShortCode.objects.filter(short_code=short_code)
        if query_short_code.exists():
            record_short_code = query_short_code.first()

        if not ShortCode.is_valid(short_code=short_code) or (record_short_code and not record_short_code.used):
            short_code = None

    # short code was not provided by the user or not accepted by the system - we get one that is generated and free
    if short_code is None:
        record_short_code = ShortCode.objects.select_for_update().filter(used__isnull=True).first()
        record_short_code.url = url

    # short code already exists in the system and can be assigned
    elif record_short_code:
        record_short_code = ShortCode.objects.select_for_update().filter(pk=record_short_code.pk).first()
        record_short_code.url = url

    # short code didnt exist before, needs to be created
    else:
        record_short_code = ShortCode(short_code=short_code, url=url)

    # supporting concurrency test scenarios to delay save in SQL (only DEBUG mode)
    if 'test_delay' in request.POST and settings.DEBUG:
        time.sleep(5)

    record_short_code.used = True
    record_short_code.save()

    return HttpResponse(record_short_code.short_code)

@transaction.atomic
def shortcode_redirect(request, short_code):
    query_short_code = ShortCode.objects.select_for_update().filter(short_code=short_code, used__isnull=False)
    if query_short_code.exists():
        record_short_code = query_short_code.first()
        record_short_code.last_redirection = timezone.now()

        # F() guarantees that counter is incremented using updated value in DB itself
        record_short_code.count_redirection = F('count_redirection') + 1
        record_short_code.save()
        return HttpResponseRedirect(record_short_code.url)
    return HttpResponseNotFound('NOT_FOUND')


def shortcode_stats(request, short_code):
    query_short_code = ShortCode.objects.filter(short_code=short_code, used__isnull=False)
    if query_short_code.exists():
        result_json = serializers.serialize('json', query_short_code)
        return HttpResponse(result_json)
    return HttpResponseNotFound('NOT_FOUND')
